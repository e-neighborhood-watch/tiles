module Demo2 exposing (main)

import Array
import Bitwise
import Browser
import Browser.Events
import Curses.Test2 as Curses
import Html exposing (Html)
import Html.Attributes
import Math.Vector3 as Vec3 exposing (Vec3,vec3)
import Task
import Time
import WebGL.Texture exposing (Texture)

type alias Flags = ()
type Message
  = ImageLoaded (Result WebGL.Texture.Error Texture)
  | TileDataGenerated (Result Curses.Error Curses.TileData)
  | Tick Time.Posix
type alias Model =
  { tileAtlas : Maybe Texture
  , count : Int
  , tileCanvas : Maybe Curses.CanvasData
  , generatedTileData : Maybe Curses.TileData
  , generatingTileData : Bool
  }

init : Flags -> ( Model, Cmd Message )
init _ =
  let
    maybeTileCanvas : Maybe Curses.CanvasData
    maybeTileCanvas =
      Curses.initialize
        { rows = 25
        , cols = 80
        , atlasRows = 16
        , atlasCols = 16
        }
  in
    ( { tileAtlas = Nothing
      , count = 0
      , tileCanvas = maybeTileCanvas
      , generatedTileData = Nothing
      , generatingTileData = True
      }
    , case maybeTileCanvas of
        Nothing ->
          Cmd.none
        Just tileCanvas ->
          Cmd.batch
            [ WebGL.Texture.loadWith
                { magnify = WebGL.Texture.nearest
                , minify = WebGL.Texture.nearest
                , horizontalWrap = WebGL.Texture.clampToEdge
                , verticalWrap = WebGL.Texture.clampToEdge
                , flipY = True
                }
                "assets/tile-atlas-packed.png"
              |> Task.attempt ImageLoaded
            , tileTable { cols = 80 } 0
              |> Array.initialize (80*25)
              |> Curses.generateTileData tileCanvas
              {-
              "assets/test-tile-data.png"
              |> WebGL.Texture.loadWith
                { magnify = WebGL.Texture.nearest
                , minify = WebGL.Texture.nearest
                , horizontalWrap = WebGL.Texture.clampToEdge
                , verticalWrap = WebGL.Texture.clampToEdge
                , flipY = True
                }
              |> Task.mapError Curses.TextureLoadError
              |> Task.map Curses.TileData
              -}
              |> Task.attempt TileDataGenerated
            ]
    )

update : Message -> Model -> ( Model, Cmd Message )
update message model =
  case message of
    ImageLoaded result ->
      case result of
        Ok texture ->
          ( { model
            | tileAtlas =
              Just texture
            }
          , Cmd.none
          )
        Err _ ->
          ( model
          , Cmd.none
          )
    TileDataGenerated result ->
      case result of
        Ok tileData ->
          ( { model
            | generatedTileData =
              Just tileData
            , generatingTileData =
              False
            }
          , Cmd.none
          )
        Err _ ->
          ( { model
            | generatingTileData =
              False
            }
          , Cmd.none
          )
    Tick _ ->
      ( { model
        | generatingTileData =
          True
        , count = model.count + 1
        }
      , if
          model.generatingTileData
        then
          Cmd.none
        else
          case model.tileCanvas of
            Nothing ->
              Cmd.none
            Just canvasData ->
              tileTable { cols = 80 } (model.count + 1)
              |> Array.initialize (80*25)
              |> Curses.generateTileData canvasData
              |> Task.attempt TileDataGenerated
      )

subscriptions : Model -> Sub Message
subscriptions { tileAtlas } =
  case tileAtlas of
    Nothing ->
      Sub.none
    Just _ ->
      Tick
      |> Browser.Events.onAnimationFrame

tileTable : { a | cols : Int } -> Int -> Int -> Curses.Tile
tileTable { cols } count ix =
  let
    uniqueColor : Vec3
    uniqueColor = createUniqueColor ix

    ured : Int
    ured =
      Vec3.getX uniqueColor
      * 256
      |> floor
      |> clamp 0 255

    ugreen : Int
    ugreen =
      Vec3.getY uniqueColor
      * 256
      |> floor
      |> clamp 0 255

    ublue : Int
    ublue =
      Vec3.getZ uniqueColor
      * 256
      |> floor
      |> clamp 0 255

    fgColor : Int
    fgColor =
      Bitwise.shiftLeftBy 16 ured
      + Bitwise.shiftLeftBy 8 ugreen
      + ublue
  in
    { fgColor =
      fgColor
    , bgColor =
      0xFFFFFF - fgColor
    , tile =
      ( ix
        // cols
        + count
        |> modBy 16
      , ix
        |> modBy cols
        |> (+) count
        |> modBy 16
      )
    }

view : Model -> Html Message
view { tileAtlas, tileCanvas, generatedTileData, count } =
  ( Maybe.map3
      Curses.render
      tileCanvas
      tileAtlas
      generatedTileData
    |> Maybe.map List.singleton
    |> Maybe.withDefault []
  )
  |> Html.div []

main : Program Flags Model Message
main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }

floatModBy : Int -> Float -> Float
floatModBy base toMod =
  let
    wholePart = floor toMod |> modBy base |> toFloat
    fracPart = toMod - (floor toMod |> toFloat)
  in wholePart + fracPart

-- From https://en.wikipedia.org/wiki/HSL_and_HSV#To_RGB
hslaToRgba : Vec3 -> Vec3
hslaToRgba hsla =
  let
    hue : Float
    hue =
      Vec3.getX hsla
    saturation : Float
    saturation =
      Vec3.getY hsla
    lightness : Float
    lightness =
      Vec3.getZ hsla
    chroma : Float
    chroma =
      (1 - abs (2*lightness - 1)) * saturation
    scaledHue : Float
    scaledHue =
      hue * 6
    minorChroma : Float
    minorChroma =
      chroma * (1 - abs (floatModBy 2 scaledHue - 1))
    lightnessShift : Float
    lightnessShift =
      lightness - chroma / 2
    lightnessShiftVector : Vec3
    lightnessShiftVector =
      vec3
        lightnessShift
        lightnessShift
        lightnessShift
  in
    Vec3.add
      lightnessShiftVector
      ( if
         scaledHue <= 1
       then
         vec3
           chroma
           minorChroma
           0
       else if
         scaledHue <= 2
       then
         vec3
           minorChroma
           chroma
           0
       else if
         scaledHue <= 3
       then
         vec3
           0
           chroma
           minorChroma
       else if
         scaledHue <= 4
       then
         vec3
           0
           minorChroma
           chroma
       else if
         scaledHue <= 5
       then
         vec3
           minorChroma
           0
           chroma
       else if
         scaledHue <= 6
       then
         vec3
           chroma
           0
           minorChroma
       else
         vec3
           0
           0
           0
      )

inversePhi : Float
inversePhi = (sqrt 5 - 1) / 2

createUniqueColor : Int -> Vec3
createUniqueColor id =
  vec3
    ( toFloat id
      * inversePhi
      |> floatModBy 1
    )
    1
    0.5
    |> hslaToRgba
