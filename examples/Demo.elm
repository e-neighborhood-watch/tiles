module Demo exposing (main)

import Browser
import Browser.Events
import Curses
import Html exposing (Html)
import Math.Vector2 exposing (Vec2,vec2)
import Task
import WebGL.Texture exposing (Texture)

type alias Flags = ()
type Message
  = ImageLoaded (Result WebGL.Texture.Error Texture)
  | Tick
type alias Model =
  { tileAtlas : Maybe Texture
  , count : Int
  }

init : Flags -> ( Model, Cmd Message )
init _ =
  ( { tileAtlas = Nothing
    , count = 0
    }
  , WebGL.Texture.load
    "assets/tile-atlas.png"
  |> Task.attempt ImageLoaded
  )

update : Message -> Model -> ( Model, Cmd Message )
update message model =
  case message of
    ImageLoaded result ->
      case result of
        Ok texture ->
          ( { model
            | tileAtlas =
              Just texture
            }
          , Cmd.none
          )
        Err _ ->
          ( model
          , Cmd.none
          )
    Tick ->
      ( { model
        | count =
          model.count + 1 |> modBy 16
        }
      , Cmd.none
      )

subscriptions : Model -> Sub Message
subscriptions { tileAtlas } =
  case tileAtlas of
    Nothing ->
      Sub.none
    Just _ ->
      Tick
      |> always
      |> Browser.Events.onAnimationFrame

tileTable : Int -> Int -> Int -> Vec2
tileTable count row col =
  Curses.ivec2 (modBy 16 col) (modBy 16 row)

view : Model -> Html Message
view { tileAtlas, count } =
  case tileAtlas of
    Nothing ->
      Html.div [] []
    Just loaded ->
      { tileAtlas = loaded
      , atlasSize = vec2 16 16
      , atlasUsedFraction = vec2 0.5625 1
      , rows = 25
      , cols = 80
      , tileTable = tileTable count
      }
      |> Curses.render (80*9) (25*16)

main : Program Flags Model Message
main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }
