module Curses exposing (..)

import Curses.Types as Type
import Html exposing (Html)
import Html.Attributes as HtmlAttribute
import Math.Vector2 exposing (Vec2, vec2)
import WebGL
import WebGL.Texture exposing (Texture)

ivec2 : Int -> Int -> Vec2
ivec2 x y = vec2 (toFloat x) (toFloat y)

vertexShader : WebGL.Shader (Type.Vertexed (Type.Tiled t)) (Type.CanvasSized (Type.TextureSized n)) { tilePos : Vec2 }
vertexShader =
  [glsl|
    attribute vec2 bottomLeft;
    attribute vec2 relPosition;
    attribute vec2 tile;
    uniform vec2 canvasSize;
    uniform vec2 atlasSize;
    uniform vec2 atlasUsedFraction;
    varying vec2 tilePos;

    vec2 rescale(vec2 pos, vec2 maxSize) {
      return 2.0*pos/maxSize - 1.0;
    }

    void main () {
      gl_Position = vec4(rescale(bottomLeft + relPosition, canvasSize), 0.0, 1.0);
      tilePos = (tile + relPosition) / atlasSize * atlasUsedFraction;
    }

  |]

fragmentShader : WebGL.Shader {} (Type.Textured f) { tilePos : Vec2 }
fragmentShader =
  [glsl|
    precision mediump float;
    varying vec2 tilePos;
    uniform sampler2D tileAtlas;

    void main () {
      gl_FragColor = texture2D(tileAtlas, tilePos);
    }
  |]

mesh : Int -> Int -> (Int -> Int -> Vec2) -> WebGL.Mesh (Type.Vertexed (Type.Tiled {}))
mesh rows cols tileTable =
  let
    square : Int -> Int -> List (Type.Triangle (Type.Vertexed (Type.Tiled {}))) -> List (Type.Triangle (Type.Vertexed (Type.Tiled {})))
    square row col triangles =
      let
        tile : Vec2
        tile = tileTable row col
      in
        ( { bottomLeft =
            ivec2 col row
          , relPosition =
            ivec2 0 0
          , tile = tile
          }
        , { bottomLeft =
            ivec2 col row
          , relPosition =
            ivec2 1 0
          , tile = tile
          }
        , { bottomLeft =
            ivec2 col row
          , relPosition =
            ivec2 0 1
          , tile = tile
          }
        )
        :: ( { bottomLeft =
            ivec2 col row
          , relPosition =
            ivec2 1 1
          , tile = tile
          }
        , { bottomLeft =
            ivec2 col row
          , relPosition =
            ivec2 0 1
          , tile = tile
          }
        , { bottomLeft =
            ivec2 col row
          , relPosition =
            ivec2 1 0
          , tile = tile
          }
        )
        :: triangles

    rowTriangles : Int -> Int -> List (Type.Triangle (Type.Vertexed (Type.Tiled {}))) -> List (Type.Triangle (Type.Vertexed (Type.Tiled {})))
    rowTriangles row col triangles =
      if
        col <= 0
      then
        triangles
      else
        let
          newCol : Int
          newCol = col - 1
        in
          rowTriangles row newCol (square row newCol triangles)

    allTriangles : Int -> List (Type.Triangle (Type.Vertexed (Type.Tiled {}))) -> List (Type.Triangle (Type.Vertexed (Type.Tiled {})))
    allTriangles row triangles =
      if
        row <= 0
      then
        triangles
      else
        let
          newRow : Int
          newRow = row - 1
        in
          allTriangles newRow (rowTriangles newRow cols triangles)

  in
    allTriangles rows []
    |> WebGL.triangles

render : Int -> Int -> Type.Curses -> Html msg
render widthPx heightPx curses =
  { canvasSize = ivec2 curses.cols curses.rows
  , tileAtlas = curses.tileAtlas
  , atlasSize = curses.atlasSize
  , atlasUsedFraction = curses.atlasUsedFraction
  } 
  |> WebGL.entityWith
    []
    vertexShader
    fragmentShader
    (mesh curses.rows curses.cols curses.tileTable)
  |> List.singleton
  |> WebGL.toHtmlWith
    [ WebGL.antialias
    ]
    [ HtmlAttribute.width widthPx
    , HtmlAttribute.height heightPx
    ]
