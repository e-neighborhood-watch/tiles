module Curses.Types exposing (..)

import Math.Vector2 exposing (Vec2)
import WebGL.Texture exposing (Texture)

type alias Curses =
  Textured
    ( TextureSized
      { tileTable : Int -> Int -> Vec2
      , rows : Int
      , cols : Int
      }
    )

type alias Triangle t = ( t, t, t )

type alias Vertexed v =
  { v
  | relPosition : Vec2
  }

type alias Tiled t =
  { t
  | bottomLeft : Vec2
  , tile : Vec2
  }

type alias Textured f =
  { f
  | tileAtlas : Texture
  }

type alias TextureSized f =
  { f
  | atlasSize : Vec2
  , atlasUsedFraction : Vec2
  }

type alias CanvasSized n =
  { n
  | canvasSize : Vec2
  }
