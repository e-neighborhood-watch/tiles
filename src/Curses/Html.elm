module Curses.Html exposing (..)

import Html exposing (Html)
import Html.Attributes as HtmlAttribute

type alias Curses = ()

render : Int -> Int -> Curses -> Html msg
render width height curses =
  let
    positiveWidth : Int
    positiveWidth = max 1 width

    positiveHeight : Int
    positiveHeight = max 1 height
  in
    Html.div [HtmlAttribute.style "margin-top" "-1px"] [ Html.text "│" ]
    |> List.repeat (positiveWidth * positiveHeight)
    |> Html.div
      [ HtmlAttribute.style "display" "grid"
      , [ "repeat("
        , height
          |> max 1
          |> String.fromInt
        , ",min-content)"
        ]
        |> String.concat
        |> HtmlAttribute.style "grid-template-rows"
      , [ "repeat("
        , width
          |> max 1
          |> String.fromInt
        , ",min-content)"
        ]
        |> String.concat
        |> HtmlAttribute.style "grid-template-columns"
      , HtmlAttribute.style "font-family" "monospace"
      ]
