module Curses.Test2 exposing (..)

import Array exposing (Array)
import Base64
import Bitwise
import Bytes exposing (Bytes)
import Bytes.Encode
import Html exposing (Html)
import Html.Attributes
import Math.Vector2 exposing (Vec2, vec2)
import Math.Vector3 exposing (Vec3)
import Task exposing (Task)
import WebGL
import WebGL.Texture exposing (Texture)

type alias Attributes a =
  { a
  | center02 : Vec2
  , canvasRelPos : Vec2
  , atlasRelPos : Vec2
  }

type alias VectorUniforms u =
  { u
  | tileData : Texture
  }

type alias FragmentUniforms u =
  { u
  | tileAtlas : Texture
  }

type alias Varyings =
  { tilePos : Vec2
  , fgColor: Vec3
  , bgColor : Vec3
  }

type alias CanvasData =
  { rows : Int
  , cols : Int
  , atlasTileWidth : Int
  , atlasTileHeight : Int
  , mesh : WebGL.Mesh (Attributes {})
  }

type alias Tile =
  { fgColor : Int
  , bgColor : Int
  , tile : (Int, Int)
  }

type TileData = TileData Texture

type alias CanvasOptions =
  { rows : Int
  , cols : Int
  , atlasRows : Int
  , atlasCols : Int
  }

type alias Triangle a = ( a, a, a )

initialize : CanvasOptions -> Maybe CanvasData
initialize { rows, cols, atlasRows, atlasCols } =
  if
    rows > 0
    && cols > 0
    && atlasRows > 0
    && atlasCols > 0
    && modBy atlasRows 256 == 0
    && modBy atlasCols 256 == 0
  then
    let
      rectWidth : Float
      rectWidth = 1 / toFloat cols

      rectHeight : Float
      rectHeight = 1 / toFloat rows

      canvasRelPosBL : Vec2
      canvasRelPosBL = vec2 -rectWidth -rectHeight

      canvasRelPosBR : Vec2
      canvasRelPosBR = vec2 rectWidth -rectHeight

      canvasRelPosTR : Vec2
      canvasRelPosTR = vec2 rectWidth rectHeight

      canvasRelPosTL : Vec2
      canvasRelPosTL = vec2 -rectWidth rectHeight

      tileWidth : Float
      tileWidth = 1 / toFloat atlasCols

      tileHeight : Float
      tileHeight = 1 / toFloat atlasRows

      atlasRelPosBL : Vec2
      atlasRelPosBL = vec2 0 0

      atlasRelPosBR : Vec2
      atlasRelPosBR = vec2 tileWidth 0

      atlasRelPosTR : Vec2
      atlasRelPosTR = vec2 tileWidth tileHeight

      atlasRelPosTL : Vec2
      atlasRelPosTL = vec2 0 tileHeight

      meshTriangles : Int -> List (Triangle (Attributes {})) -> List (Triangle (Attributes {}))
      meshTriangles row prevRows =
        if
          row <= 0
        then
          prevRows
        else
          let
            meshRow : Int -> List (Triangle (Attributes {})) -> List (Triangle (Attributes {}))
            meshRow col prev =
              if
                col <= 0
              then
                prev
              else
                let
                  center02 : Vec2
                  center02 =
                    vec2
                      ((toFloat (2*col - 1)) / toFloat cols)
                      ((toFloat (2*row - 1)) / toFloat rows)
                in
                  ( { center02 = center02
                    , canvasRelPos = canvasRelPosBL
                    , atlasRelPos = atlasRelPosBL
                    }
                  , { center02 = center02
                    , canvasRelPos = canvasRelPosBR
                    , atlasRelPos = atlasRelPosBR
                    }
                  , { center02 = center02
                    , canvasRelPos = canvasRelPosTL
                    , atlasRelPos = atlasRelPosTL
                    }
                  )
                  ::
                    ( { center02 = center02
                      , canvasRelPos = canvasRelPosTR
                      , atlasRelPos = atlasRelPosTR
                      }
                    , { center02 = center02
                      , canvasRelPos = canvasRelPosTL
                      , atlasRelPos = atlasRelPosTL
                      }
                    , { center02 = center02
                      , canvasRelPos = canvasRelPosBR
                      , atlasRelPos = atlasRelPosBR
                      }
                    )
                  :: prev
                  |> meshRow (col - 1)
          in
            prevRows
            |> meshRow cols
            |> meshTriangles (row - 1)
    in
      Just
        { rows = rows
        , cols = cols
        , atlasTileWidth = 256 // atlasCols
        , atlasTileHeight = 256 // atlasRows
        , mesh =
          meshTriangles rows []
          |> WebGL.triangles
        }
  else
    Nothing

render : CanvasData -> Texture -> TileData -> Html a
render canvasData tileAtlas (TileData tileData) =
  WebGL.entityWith
    []
    vertexShader
    fragmentShader
    canvasData.mesh
    { tileData = tileData
    , tileAtlas = tileAtlas
    }
  |> List.singleton
  |> WebGL.toHtmlWith
    [ WebGL.antialias
    ]
    [ Html.Attributes.width (9*80)
    , Html.Attributes.height (25*16)
    ]

crcGenerator : Int
crcGenerator = 0xedb88320

calculateByteCrc : Int -> Int
calculateByteCrc x =
  let
    crcStep : Int -> Int
    crcStep y =
      y
      |> Bitwise.shiftRightZfBy 1
      |> if
          Bitwise.and y 0x1 == 1
        then
          Bitwise.xor crcGenerator
        else
          identity
  in
    crcStep
    |> List.repeat 8
    |> List.foldl (<|) x

crcTable : Array Int
crcTable =
  Array.initialize 256 calculateByteCrc

lookupCrc : Int -> Int
lookupCrc x =
  case Array.get x crcTable of
    Just result -> result
    Nothing -> calculateByteCrc x

crc32AndLength : ChunkType -> List ChunkData -> { length : Int, crc : Int }
crc32AndLength chunkType =
  let
    chunkDataToBytes : ChunkData -> { length : Int, bytes : List Int }
    chunkDataToBytes chunkData =
      case chunkData of
        OneByte byte ->
          { length =
            1
          , bytes =
            [ byte
            ]
          }
        TwoByte endianness bytes ->
          { length =
            2
          , bytes =
            case endianness of
              Bytes.LE ->
                [ bytes
                , Bitwise.shiftRightZfBy 8 bytes
                ]
              Bytes.BE ->
                [ Bitwise.shiftRightZfBy 8 bytes
                , bytes
                ]
          }
        FourByte bytes ->
          { length =
            4
          , bytes =
            [ Bitwise.shiftRightZfBy 24 bytes
            , Bitwise.shiftRightZfBy 16 bytes
            , Bitwise.shiftRightZfBy 8 bytes
            , bytes
            ]
          }

    startingCrc : Int
    startingCrc =
      case chunkType of
        IHDR ->
          0x575E51F5
        IDAT ->
          0xCA50F9E1

    crcByte : Int -> Int -> Int
    crcByte byte crc =
      byte
      |> Bitwise.xor crc
      |> Bitwise.and 0xFF
      |> lookupCrc
      |> Bitwise.xor (Bitwise.shiftRightZfBy 8 crc)

    processBytesAndLength : { a | length : Int, bytes : List Int } -> { b | length : Int, crc : Int } -> { b | length : Int, crc : Int }
    processBytesAndLength new prev =
      { prev
      | length = prev.length + new.length
      , crc = List.foldl crcByte prev.crc new.bytes
      }

    overCrc : (b -> b) -> { a | crc : b } -> { a | crc : b }
    overCrc f crcRec =
      { crcRec
      | crc = f crcRec.crc
      }
  in
    List.foldl
      (chunkDataToBytes >> processBytesAndLength)
      { length = 0
      , crc = startingCrc
      }
    >> overCrc Bitwise.complement

type ChunkType
  = IHDR
  | IDAT

type ChunkData
  = OneByte Int
  | TwoByte Bytes.Endianness Int
  | FourByte Int

pngChunk : ChunkType -> List ChunkData -> Bytes.Encode.Encoder
pngChunk chunkType data =
  let
    crcAndLength : { length : Int, crc : Int }
    crcAndLength = crc32AndLength chunkType data

    chunkTypeString : String
    chunkTypeString =
      case chunkType of
        IHDR ->
          "IHDR"
        IDAT ->
          "IDAT"

    chunkDataToEncoder : ChunkData -> Bytes.Encode.Encoder
    chunkDataToEncoder chunkData =
      case chunkData of
        OneByte byte ->
          Bytes.Encode.unsignedInt8 byte
        TwoByte endianness bytes ->
          Bytes.Encode.unsignedInt16 endianness bytes
        FourByte bytes ->
          Bytes.Encode.unsignedInt32 Bytes.BE bytes
  in
    Bytes.Encode.sequence
      [ Bytes.Encode.unsignedInt32 Bytes.BE crcAndLength.length
      , Bytes.Encode.string chunkTypeString
      , data |> List.map chunkDataToEncoder |> Bytes.Encode.sequence
      , Bytes.Encode.unsignedInt32 Bytes.BE crcAndLength.crc
      ]

type Error
  = TextureLoadError WebGL.Texture.Error
  | TooManyTiles

type alias DataBytesIntermediate =
  { s1 : Int
  , s2 : Int
  , revChunkData : List ChunkData
  }

dataToBytes : CanvasData -> Array Tile -> List ChunkData
dataToBytes { rows, cols, atlasTileWidth, atlasTileHeight } data =
  let
    processRow : Bool -> Tile -> DataBytesIntermediate -> DataBytesIntermediate
    processRow fgOrBg { fgColor, bgColor, tile } =
      let
        colorToBytes : Int -> Int -> DataBytesIntermediate -> DataBytesIntermediate
        colorToBytes color tileIx prev =
          let
            red : Int
            red =
              color
              |> Bitwise.shiftRightBy 16
              |> Bitwise.and 0xFF

            green : Int
            green =
              color
              |> Bitwise.shiftRightBy 8
              |> Bitwise.and 0xFF

            blue : Int
            blue =
              color
              |> Bitwise.and 0xFF

            boundedTileIx : Int
            boundedTileIx =
              tileIx
              |> Bitwise.and 0xFF
          in
            { prev
            | s1 = modBy 65521 (prev.s1 + red + green + blue + boundedTileIx)
            , s2 = modBy 65521 (prev.s2 + 4*(prev.s1 + red) + 3*green + 2*blue + boundedTileIx)
            , revChunkData = OneByte boundedTileIx :: OneByte blue :: OneByte green :: OneByte red :: prev.revChunkData
            }
      in
        if
          fgOrBg
        then
          colorToBytes fgColor (Tuple.second tile * atlasTileWidth)
        else
          colorToBytes bgColor (Tuple.first tile * atlasTileHeight)


    byRow : Bool -> Int -> Array Tile -> DataBytesIntermediate -> DataBytesIntermediate
    byRow fgOrBg row arr prev =
      if
        row <= 0
      then
        prev
      else
        let
          currentRow : Array Tile
          currentRow =
            Array.slice 0 cols arr

          remainingTiles : Array Tile
          remainingTiles =
            Array.slice cols (Array.length arr) arr
        in
          currentRow
          |>
            Array.foldl
              ( processRow fgOrBg )
              { prev
              | s2 = modBy 65521 (prev.s2 + prev.s1)
              , revChunkData = OneByte 0x0 :: prev.revChunkData
              }
          |> byRow fgOrBg (row - 1) remainingTiles

    finalResult : DataBytesIntermediate
    finalResult =
      { s1 = 1
      , s2 = 0
      , revChunkData = []
      }
      |> byRow True rows data
      |> byRow False rows data
  in
    ( finalResult.s2
      |> Bitwise.shiftLeftBy 16
      |> Bitwise.or finalResult.s1
      |> FourByte
    )
    :: finalResult.revChunkData
    |> List.reverse

generateBase64String : CanvasData -> Array Tile -> Maybe String
generateBase64String canvasData data =
  let
    dataBytes : Int
    dataBytes = (4 * canvasData.rows + 1) * canvasData.cols * 2
  in
    if
      dataBytes > 65535
    then
      Nothing
    else
      let
        png : Bytes
        png =
          Bytes.Encode.sequence
            -- PNG signature
            [ Bytes.Encode.unsignedInt8 0x89
            , Bytes.Encode.string "PNG"
            , Bytes.Encode.unsignedInt8 0x0D
            , Bytes.Encode.unsignedInt8 0x0A
            , Bytes.Encode.unsignedInt8 0x1A
            , Bytes.Encode.unsignedInt8 0x0A
            -- Image Header
            , pngChunk
              IHDR
              [ FourByte canvasData.cols -- Width
              , FourByte (canvasData.rows * 2) -- Height
              , OneByte 8     -- Bit depth
              , OneByte 6     -- Color type
              , OneByte 0     -- Compression Method
              , OneByte 0     -- Filter method
              , OneByte 0     -- Interlace method
              ]
            -- Image Data
            , OneByte 0x08
              :: OneByte 0x1D
              :: OneByte 0x01
              :: TwoByte Bytes.LE dataBytes
              :: ( dataBytes |> Bitwise.complement |> TwoByte Bytes.LE )
              :: dataToBytes canvasData data
              |> pngChunk IDAT
            -- Image End
            , Bytes.Encode.unsignedInt32 Bytes.BE 0x00000000
            , Bytes.Encode.string "IEND"
            , Bytes.Encode.unsignedInt32 Bytes.BE 0xAE426082
            ]
          |> Bytes.Encode.encode
      in
        String.concat
          [ "data:image/png;base64,"
          , png
          |> Base64.fromBytes
          |> Maybe.withDefault ""
          ]
        |> Just

generateTileData : CanvasData -> Array Tile -> Task Error TileData
generateTileData canvasData data =
  case generateBase64String canvasData data of
    Nothing ->
      Task.fail TooManyTiles
    Just pngString ->
      pngString
      |> WebGL.Texture.loadWith
        { magnify = WebGL.Texture.nearest
        , minify = WebGL.Texture.nearest
        , horizontalWrap = WebGL.Texture.clampToEdge
        , verticalWrap = WebGL.Texture.clampToEdge
        , flipY = True
        }
      |> Task.map TileData
      |> Task.mapError TextureLoadError

vertexShader : WebGL.Shader (Attributes a) (VectorUniforms u) Varyings
vertexShader =
  [glsl|
    attribute vec2 center02;
    attribute vec2 canvasRelPos;
    attribute vec2 atlasRelPos;

    uniform sampler2D tileData;

    varying vec2 tilePos;
    varying vec3 fgColor;
    varying vec3 bgColor;

    void main () {
      vec2 halfCenter = vec2(center02.x, center02.y / 2.0) / 2.0;
      vec4 data2 = texture2D(tileData, halfCenter);
      vec4 data1 = texture2D(tileData, halfCenter + vec2(0.0, 0.5));
      fgColor = data1.rgb;
      bgColor = data2.rgb;
      tilePos = vec2(data1.a, data2.a)*0.99609375 + atlasRelPos;
      gl_Position = vec4(center02 - 1.0 + canvasRelPos, 0.0, 1.0);
    }
  |]

fragmentShader : WebGL.Shader {} (FragmentUniforms u) Varyings
fragmentShader =
  [glsl|
    precision mediump float;
    uniform sampler2D tileAtlas;

    varying vec2 tilePos;
    varying vec3 fgColor;
    varying vec3 bgColor;

    void main () {
      vec4 tile = texture2D(tileAtlas, tilePos);
      gl_FragColor = vec4(mix(bgColor, fgColor * tile.rgb, tile.a), 1.0);
    }
  |]

