module Curses.Test exposing (..)

import Curses.Types as Type
import Html exposing (Html)
import Html.Attributes as HtmlAttribute
import Math.Vector2 exposing (Vec2, vec2)
import WebGL

ivec2 : Int -> Int -> Vec2
ivec2 x y = vec2 (toFloat x) (toFloat y)

vertexShader : WebGL.Shader (Type.Vertexed v) (Type.TextureSized (Type.CanvasSized (Type.Tiled n))) { tilePos : Vec2 }
vertexShader =
  [glsl|
    attribute vec2 relPosition;
    uniform vec2 bottomLeft;
    uniform vec2 tile;
    uniform vec2 canvasSize;
    uniform vec2 atlasSize;
    uniform vec2 atlasUsedFraction;
    varying vec2 tilePos;

    vec2 rescale(vec2 pos, vec2 maxSize) {
      return 2.0*pos/maxSize - 1.0;
    }

    void main () {
      gl_Position = vec4(rescale(bottomLeft + relPosition, canvasSize), 0.0, 1.0);
      tilePos = (tile + relPosition) / atlasSize * atlasUsedFraction;
    }

  |]

fragmentShader : WebGL.Shader {} (Type.Textured f) { tilePos : Vec2 }
fragmentShader =
  [glsl|
    precision mediump float;
    varying vec2 tilePos;
    uniform sampler2D tileAtlas;

    void main () {
      gl_FragColor = texture2D(tileAtlas, tilePos);
    }
  |]

entityList : Int -> Int -> (Int -> Int -> Vec2) -> Type.CanvasSized (Type.TextureSized (Type.Textured n)) -> List WebGL.Entity
entityList rows cols tileTable uniform =
  let
    square : Int -> Int -> List WebGL.Entity -> List WebGL.Entity
    square row col entities =
      WebGL.entity
        vertexShader
        fragmentShader
        ( WebGL.indexedTriangles
          [ { relPosition =
              ivec2 0 0
            }
          , { relPosition =
              ivec2 1 0
            }
          , { relPosition =
              ivec2 0 1
            }
          , { relPosition =
              ivec2 1 1
            }
          ]
          [ ( 0, 1, 2 )
          , ( 3, 2, 1 )
          ]
        )
        { tileAtlas = uniform.tileAtlas
        , atlasSize = uniform.atlasSize
        , atlasUsedFraction = uniform.atlasUsedFraction
        , canvasSize = uniform.canvasSize
        , bottomLeft = ivec2 col row
        , tile = tileTable row col
        }
      :: entities

    rowEntities : Int -> Int -> List WebGL.Entity -> List WebGL.Entity
    rowEntities row col entities =
      if
        col <= 0
      then
        entities
      else
        let
          newCol : Int
          newCol = col - 1
        in
          rowEntities row newCol (square row newCol entities)

    allEntities : Int -> List WebGL.Entity -> List WebGL.Entity
    allEntities row entities =
      if
        row <= 0
      then
        entities
      else
        let
          newRow : Int
          newRow = row - 1
        in
          allEntities newRow (rowEntities newRow cols entities)

  in
    allEntities rows []

render : Int -> Int -> Type.Curses -> Html msg
render widthPx heightPx curses =
  { canvasSize = ivec2 curses.cols curses.rows
  , tileAtlas = curses.tileAtlas
  , atlasSize = curses.atlasSize
  , atlasUsedFraction = curses.atlasUsedFraction
  } 
  |> entityList curses.rows curses.cols curses.tileTable
  |> WebGL.toHtml
    [ HtmlAttribute.width widthPx
    , HtmlAttribute.height heightPx
    ]
