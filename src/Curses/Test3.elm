module Curses.Test3 exposing (..)

import Array exposing (Array)
import Base64
import Bitwise
import Bytes exposing (Bytes)
import Bytes.Encode
import Html exposing (Html)
import Html.Attributes
import Math.Vector2 as Vec2 exposing (Vec2, vec2)
import Task exposing (Task)
import WebGL
import WebGL.Texture exposing (Texture)


type alias Attributes a =
  { a
  | vpos : Vec2
  }


type alias VectorUniforms u =
  { u
  | canvasSize : Vec2
  }


type alias FragmentUniforms u =
  { u
  | tileAtlas : Texture
  , tileData : Texture
  , canvasSize : Vec2
  , atlasSize : Vec2
  }


type alias Varyings a =
  { a
  | fpos : Vec2
  }


type alias Canvas =
  { tiles : Array Tile
  , rows : Int
  , cols : Int
  }


type TileAtlas =
  TileAtlas
    { tileAtlas : Texture
    , size : Vec2
    , tileSize : (Int, Int)
    }


type alias Tile =
  { fgColor : Int
  , bgColor : Int
  , tile : (Int, Int)
  }

type TileData =
  TileData
    { tileData : Texture
    , tileAtlas : Texture
    , canvasSize : Vec2
    , atlasSize : Vec2
    }


mesh : WebGL.Mesh (Attributes {})
mesh =
  WebGL.indexedTriangles
    [ { vpos = vec2 -1 -1
      }
    , { vpos = vec2 1 -1
      }
    , { vpos = vec2 1 1
      }
    , { vpos = vec2 -1 1
      }
    ]
    [ ( 0, 1, 3 )
    , ( 2, 3, 1 )
    ]

render : TileData -> Html a
render (TileData tileData) =
  let
    atlasTextureSize : (Int, Int)
    atlasTextureSize =
      WebGL.Texture.size tileData.tileAtlas

    htmlCanvasWidth : Int
    htmlCanvasWidth =
      Tuple.first atlasTextureSize
      // round (Vec2.getX tileData.atlasSize)
      * round (Vec2.getX tileData.canvasSize)

    htmlCanvasHeight : Int
    htmlCanvasHeight =
      Tuple.second atlasTextureSize
      // round (Vec2.getY tileData.atlasSize)
      * round (Vec2.getY tileData.canvasSize)
  in
    WebGL.entityWith
      [ ]
      vertexShader
      fragmentShader
      mesh
      tileData
    |> List.singleton
    |> WebGL.toHtmlWith
      [ ]
      [ Html.Attributes.width htmlCanvasWidth
      , Html.Attributes.height htmlCanvasHeight
      ]

crcGenerator : Int
crcGenerator = 0xedb88320

calculateByteCrc : Int -> Int
calculateByteCrc x =
  let
    crcStep : Int -> Int
    crcStep y =
      y
      |> Bitwise.shiftRightZfBy 1
      |> if
          Bitwise.and y 0x1 == 1
        then
          Bitwise.xor crcGenerator
        else
          identity
  in
    crcStep
    |> List.repeat 8
    |> List.foldl (<|) x

crcTable : Array Int
crcTable =
  Array.initialize 256 calculateByteCrc

lookupCrc : Int -> Int
lookupCrc x =
  case Array.get x crcTable of
    Just result -> result
    Nothing -> calculateByteCrc x

crc32AndLength : ChunkType -> List ChunkData -> { length : Int, crc : Int }
crc32AndLength chunkType =
  let
    chunkDataToBytes : ChunkData -> { length : Int, bytes : List Int }
    chunkDataToBytes chunkData =
      case chunkData of
        OneByte byte ->
          { length =
            1
          , bytes =
            [ byte
            ]
          }
        TwoByte endianness bytes ->
          { length =
            2
          , bytes =
            case endianness of
              Bytes.LE ->
                [ bytes
                , Bitwise.shiftRightZfBy 8 bytes
                ]
              Bytes.BE ->
                [ Bitwise.shiftRightZfBy 8 bytes
                , bytes
                ]
          }
        FourByte bytes ->
          { length =
            4
          , bytes =
            [ Bitwise.shiftRightZfBy 24 bytes
            , Bitwise.shiftRightZfBy 16 bytes
            , Bitwise.shiftRightZfBy 8 bytes
            , bytes
            ]
          }

    startingCrc : Int
    startingCrc =
      case chunkType of
        IHDR ->
          0x575E51F5
        IDAT ->
          0xCA50F9E1

    crcByte : Int -> Int -> Int
    crcByte byte crc =
      byte
      |> Bitwise.xor crc
      |> Bitwise.and 0xFF
      |> lookupCrc
      |> Bitwise.xor (Bitwise.shiftRightZfBy 8 crc)

    processBytesAndLength : { a | length : Int, bytes : List Int } -> { b | length : Int, crc : Int } -> { b | length : Int, crc : Int }
    processBytesAndLength new prev =
      { prev
      | length = prev.length + new.length
      , crc = List.foldl crcByte prev.crc new.bytes
      }

    overCrc : (b -> b) -> { a | crc : b } -> { a | crc : b }
    overCrc f crcRec =
      { crcRec
      | crc = f crcRec.crc
      }
  in
    List.foldl
      (chunkDataToBytes >> processBytesAndLength)
      { length = 0
      , crc = startingCrc
      }
    >> overCrc Bitwise.complement

type ChunkType
  = IHDR
  | IDAT

type ChunkData
  = OneByte Int
  | TwoByte Bytes.Endianness Int
  | FourByte Int

pngChunk : ChunkType -> List ChunkData -> Bytes.Encode.Encoder
pngChunk chunkType data =
  let
    crcAndLength : { length : Int, crc : Int }
    crcAndLength = crc32AndLength chunkType data

    chunkTypeString : String
    chunkTypeString =
      case chunkType of
        IHDR ->
          "IHDR"
        IDAT ->
          "IDAT"

    chunkDataToEncoder : ChunkData -> Bytes.Encode.Encoder
    chunkDataToEncoder chunkData =
      case chunkData of
        OneByte byte ->
          Bytes.Encode.unsignedInt8 byte
        TwoByte endianness bytes ->
          Bytes.Encode.unsignedInt16 endianness bytes
        FourByte bytes ->
          Bytes.Encode.unsignedInt32 Bytes.BE bytes
  in
    Bytes.Encode.sequence
      [ Bytes.Encode.unsignedInt32 Bytes.BE crcAndLength.length
      , Bytes.Encode.string chunkTypeString
      , data |> List.map chunkDataToEncoder |> Bytes.Encode.sequence
      , Bytes.Encode.unsignedInt32 Bytes.BE crcAndLength.crc
      ]

type TileDataError
  = TileDataTextureLoadError WebGL.Texture.Error
  | TooManyTiles
  | NonPositiveCanvasSize
  | IncorrectCanvasSize


type LoadAtlasError
  = AtlasTextureLoadError WebGL.Texture.Error
  | NonPositiveAtlasSize
  | AtlasSizeDoesNotDivide256
  | AtlasSizeDoesNotDivideTextureSize ( Int, Int )


type alias DataBytesIntermediate =
  { s1 : Int
  , s2 : Int
  , revChunkData : List ChunkData
  }

dataToBytes : (Int, Int) -> Canvas -> List ChunkData
dataToBytes (atlasTileWidth, atlasTileHeight) canvas =
  let
    processRow : Bool -> Tile -> DataBytesIntermediate -> DataBytesIntermediate
    processRow fgOrBg { fgColor, bgColor, tile } =
      let
        colorToBytes : Int -> Int -> DataBytesIntermediate -> DataBytesIntermediate
        colorToBytes color tileIx prev =
          let
            red : Int
            red =
              color
              |> Bitwise.shiftRightBy 16
              |> Bitwise.and 0xFF

            green : Int
            green =
              color
              |> Bitwise.shiftRightBy 8
              |> Bitwise.and 0xFF

            blue : Int
            blue =
              color
              |> Bitwise.and 0xFF

            boundedTileIx : Int
            boundedTileIx =
              tileIx
              |> Bitwise.and 0xFF
          in
            { prev
            | s1 = modBy 65521 (prev.s1 + red + green + blue + boundedTileIx)
            , s2 = modBy 65521 (prev.s2 + 4*(prev.s1 + red) + 3*green + 2*blue + boundedTileIx)
            , revChunkData = OneByte boundedTileIx :: OneByte blue :: OneByte green :: OneByte red :: prev.revChunkData
            }
      in
        if
          fgOrBg
        then
          colorToBytes fgColor (Tuple.second tile * atlasTileWidth)
        else
          colorToBytes bgColor (Tuple.first tile * atlasTileHeight)


    byRow : Bool -> Int -> Array Tile -> DataBytesIntermediate -> DataBytesIntermediate
    byRow fgOrBg row arr prev =
      if
        row <= 0
      then
        prev
      else
        let
          currentRow : Array Tile
          currentRow =
            Array.slice 0 canvas.cols arr

          remainingTiles : Array Tile
          remainingTiles =
            Array.slice canvas.cols (Array.length arr) arr
        in
          currentRow
          |>
            Array.foldl
              ( processRow fgOrBg )
              { prev
              | s2 = modBy 65521 (prev.s2 + prev.s1)
              , revChunkData = OneByte 0x0 :: prev.revChunkData
              }
          |> byRow fgOrBg (row - 1) remainingTiles

    finalResult : DataBytesIntermediate
    finalResult =
      { s1 = 1
      , s2 = 0
      , revChunkData = []
      }
      |> byRow True canvas.rows canvas.tiles
      |> byRow False canvas.rows canvas.tiles
  in
    ( finalResult.s2
      |> Bitwise.shiftLeftBy 16
      |> Bitwise.or finalResult.s1
      |> FourByte
    )
    :: finalResult.revChunkData
    |> List.reverse

generateBase64String : ( Int, Int ) -> Canvas  -> Maybe String
generateBase64String atlasTileSize canvas =
  let
    dataBytes : Int
    dataBytes = (4 * canvas.rows + 1) * canvas.cols * 2
  in
    if
      dataBytes > 65535
    then
      Nothing
    else
      let
        png : Bytes
        png =
          Bytes.Encode.sequence
            -- PNG signature
            [ Bytes.Encode.unsignedInt8 0x89
            , Bytes.Encode.string "PNG"
            , Bytes.Encode.unsignedInt8 0x0D
            , Bytes.Encode.unsignedInt8 0x0A
            , Bytes.Encode.unsignedInt8 0x1A
            , Bytes.Encode.unsignedInt8 0x0A
            -- Image Header
            , pngChunk
              IHDR
              [ FourByte canvas.cols -- Width
              , FourByte (canvas.rows * 2) -- Height
              , OneByte 8     -- Bit depth
              , OneByte 6     -- Color type
              , OneByte 0     -- Compression Method
              , OneByte 0     -- Filter method
              , OneByte 0     -- Interlace method
              ]
            -- Image Data
            , OneByte 0x08
              :: OneByte 0x1D
              :: OneByte 0x01
              :: TwoByte Bytes.LE dataBytes
              :: ( dataBytes |> Bitwise.complement |> TwoByte Bytes.LE )
              :: dataToBytes atlasTileSize canvas
              |> pngChunk IDAT
            -- Image End
            , Bytes.Encode.unsignedInt32 Bytes.BE 0x00000000
            , Bytes.Encode.string "IEND"
            , Bytes.Encode.unsignedInt32 Bytes.BE 0xAE426082
            ]
          |> Bytes.Encode.encode
      in
        String.concat
          [ "data:image/png;base64,"
          , png
          |> Base64.fromBytes
          |> Maybe.withDefault ""
          ]
        |> Just


loadTileAtlas : { rows : Int, cols : Int } -> String -> Task LoadAtlasError TileAtlas
loadTileAtlas { rows, cols } src =
  if
    rows <= 0
    || cols <= 0
  then
    Task.fail NonPositiveAtlasSize
  else
    if
      modBy rows 256 /= 0
      || modBy cols 256 /= 0
    then
      Task.fail AtlasSizeDoesNotDivide256
    else
      WebGL.Texture.loadWith
        { magnify = WebGL.Texture.nearest
        , minify = WebGL.Texture.nearest
        , horizontalWrap = WebGL.Texture.clampToEdge
        , verticalWrap = WebGL.Texture.clampToEdge
        , flipY = True
        }
        src
      |> Task.mapError AtlasTextureLoadError
      |>
        Task.andThen
          ( \ atlasTexture ->
            if
              atlasTexture
              |> WebGL.Texture.size
              |> Tuple.mapBoth (modBy cols >> (/=) 0) (modBy rows >> (/=) 0)
              |> ( \ ( a, b ) -> a || b )
            then
              atlasTexture
              |> WebGL.Texture.size
              |> AtlasSizeDoesNotDivideTextureSize 
              |> Task.fail
            else
              { tileSize = ( 256 // cols, 256 // rows )
              , size = vec2 (toFloat cols) (toFloat rows)
              , tileAtlas = atlasTexture
              }
              |> TileAtlas
              |> Task.succeed
          )


generateTileData : TileAtlas -> Canvas -> Task TileDataError TileData
generateTileData (TileAtlas tileAtlas) canvas =
  if
    canvas.rows <= 0
    || canvas.cols <= 0
  then
    Task.fail NonPositiveCanvasSize
  else
    if
      canvas.rows
      * canvas.cols
      /= Array.length canvas.tiles
    then
      Task.fail IncorrectCanvasSize
    else
      case generateBase64String tileAtlas.tileSize canvas of
        Nothing ->
          Task.fail TooManyTiles
        Just pngString ->
          pngString
          |> WebGL.Texture.loadWith
            { magnify = WebGL.Texture.nearest
            , minify = WebGL.Texture.nearest
            , horizontalWrap = WebGL.Texture.clampToEdge
            , verticalWrap = WebGL.Texture.clampToEdge
            , flipY = True
            }
          |>
            Task.map
              ( \ tileData ->
                TileData
                  { tileData = tileData
                  , tileAtlas = tileAtlas.tileAtlas
                  , atlasSize = tileAtlas.size
                  , canvasSize = vec2 (toFloat canvas.cols) (toFloat canvas.rows)
                  }
              )
          |> Task.mapError TileDataTextureLoadError


vertexShader : WebGL.Shader (Attributes a) (VectorUniforms u) (Varyings {})
vertexShader =
  [glsl|
    attribute vec2 vpos;

    varying vec2 fpos;

    void main () {
      fpos = (1.0 + vpos) / 2.0;
      gl_Position = vec4(vpos, 0.0, 1.0);
    }
  |]


fragmentShader : WebGL.Shader {} (FragmentUniforms u) (Varyings {})
fragmentShader =
  [glsl|
    precision mediump float;
    uniform sampler2D tileAtlas;
    uniform sampler2D tileData;
    uniform vec2 canvasSize;
    uniform vec2 atlasSize;

    varying vec2 fpos;

    void main () {
      vec2 halfPos = fpos * vec2(1.0, 0.5);
      vec4 data1 = texture2D(tileData, halfPos + vec2(0.0, 0.5));
      vec4 data2 = texture2D(tileData, halfPos);
      vec4 tile = texture2D(tileAtlas, vec2(data1.a, data2.a)*0.99609375 + fract(fpos*canvasSize)/atlasSize);
      gl_FragColor = vec4(mix(data2.rgb, data1.rgb * tile.rgb, tile.a), 1.0);
    }
  |]

